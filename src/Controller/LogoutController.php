<?php

namespace Drupal\syncabinet\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Cart JSON-DATA: only load cart info.
 */
class LogoutController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function logout() {
    if ($this->currentUser()->isAuthenticated()) {
      user_logout();
    }
    return $this->redirect('<front>');
  }

}
