<?php

namespace Drupal\syncabinet\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SubmitPage Controller.
 */
class SubmitPageController extends ControllerBase {

  /**
   * Previous Routname.
   */
  public function prevRoutname() {
    $previousUrl = \Drupal::request()->server->get('HTTP_REFERER');
    $fake_request = Request::create($previousUrl);
    $url_object = \Drupal::service('path.validator')->getUrlIfValid($fake_request->getRequestUri());
    if ($url_object) {
      $route_name = $url_object->getRouteName();
      return $route_name;
    }
    return "NO PREVIOUS_ROUTNAME";
  }

  /**
   * Page.
   */
  public function page() {
    $previous_routname = $this->prevRoutname();
    $output = [];
    $title = '';
    $text = '';
    if ($previous_routname == 'user.register') {
      $title = "<h1 class='submit-result-title'>Подтверждение регистрации</h1>";
      $text = "<p class='submit-result-text'>На ваш email было отправлено приветственное письмо с дальнейшими указаниями. Если вы не получили его в течение 5 минут - проверьте папку 'Спам'.</p>";
    }
    elseif ($previous_routname == 'user.pass') {
      $title = "<h1 class='submit-result-title'>Изменение пароля</h1>";
      $text = "<p class='submit-result-text'>Инструкция по изменению пароля отправлена на указанный email.</p>";
    }
    $output['page'] = [
      '#prefix' => "<div class='submit-result'>",
      '#markup' => $title . $text,
      '#suffix' => "</div>",
    ];
    return $output;
  }

}
