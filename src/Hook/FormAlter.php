<?php

namespace Drupal\syncabinet\Hook;


/**
 * Hook form alter.
 */
class FormAlter {

  /**
   * Implements hook_form_alter().
   */
  public static function hook(&$form, &$form_state) {
    $forms = [
      'user_pass',
      'user_form',
      'user_login_form',
      'user_register_form',
    ];
    if (in_array($form['#form_id'], $forms)) {
      $form['#attributes']['class'][] = 'contact-message-form';
      $form['#attributes']['class'][] = 'cabinet-form';
    }
    if ($form['#form_id'] == 'user_form') {
      $form['#prefix'] = '<h1 class="user-login-title">Изменение данных пользователя</h1>';
      $form['actions']['submit']['#submit'][] = 'syncabinet_user_form_submit';
    }
    if ($form['#form_id'] == 'user_pass') {
      $form['#prefix'] = '<h1 class="user-login-title">Изменение пароля</h1>';
      $form['name']['#suffix'] = '<div class="user-login-description">Укажите адрес, на который будет отправлена инструкция по изменению пароля</div>';
    }
    if ($form['#form_id'] == 'user_login_form') {
      $form['#prefix'] = '<h1 class="user-login-title">Вход в личный кабинет</h1>';
      $form['pass']['#suffix'] = '<a class="user-login-forgot-link" href="/user/password">Забыли пароль?</a>';
      $form['actions']['#prefix'] = '<div class="user-login-actions-group">';
      $form['actions']['#suffix'] = '<span>или</span><a class="user-login-registration-link" href="/user/register">Регистрация</a></div>';
    }
    if ($form['#form_id'] == 'user_register_form') {
      $form['actions']['submit']['#submit'][] = 'syncabinet_user_register_form_submit';
    }
  }
}
