<?php

namespace Drupal\syncabinet\Hook;

/**
 * Hook preprocess page.
 */
class PreprocessCommerceOrder {

  /**
   * Implements hook_preprocess_commerce_order().
   */
  public static function hook(&$variables) {
    $variables['repeat_order'] = TRUE;
  }

}
