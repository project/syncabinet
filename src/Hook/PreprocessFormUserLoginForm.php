<?php

namespace Drupal\syncabinet\Hook;

/**
 * Hook preprocess page.
 */
class PreprocessFormUserLoginForm {

  /**
   * Implements hook_preprocess_form().
   */
  public static function hook(&$variables) {
    $vkontakte = \Drupal::config('social_auth_vk.settings');
    $facebook = \Drupal::config('social_auth_facebook.settings');
    $variables['uid'] = \Drupal::currentUser()->id();
    $variables['vkontakte_id'] = $vkontakte->get('client_id');
    $variables['facebook_id'] = $facebook->get('app_id');
    template_preprocess_form($variables);
  }

}
