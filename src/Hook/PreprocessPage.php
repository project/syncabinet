<?php

namespace Drupal\syncabinet\Hook;

/**
 * Hook preprocess page.
 */
class PreprocessPage {

  /**
   * Implements hook_preprocess_page().
   */
  public static function hook(&$variables) {
    $variables['#attached']['library'][] = 'syncabinet/base';
  }

}
