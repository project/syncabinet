<?php

namespace Drupal\syncabinet\Hook;

/**
 * ThemeHooks.
 */
class Theme {

  /**
   * Implements hook_theme().
   */
  public static function hook() {
    return [
      'form__user_login_form' => [
        'render element' => 'elements',
        'template' => 'form--user-login-form',
      ],
      'form__user_register_form' => [
        'render element' => 'elements',
        'template' => 'form--user-register-form',
      ],
    ];
  }

}
