<?php

namespace Drupal\syncabinet\Hook;

/**
 * Hook preprocess page.
 */
class ThemeSuggestionsFormAlter {

  /**
   * Implements hook_preprocess_page().
   */
  public static function hook(&$suggestions, $variables) {
    $suggestions[] = 'form__' . $variables['element']['#form_id'];
  }

}
